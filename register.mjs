import { Async } from 'hyphae/async'
import { Sync } from 'hyphae/sync'
import { pipeAttr, pipeText, pipeProp, pipeChildren, frame } from '.'

// Pipe attribute
Async.prototype.pipeAttr = function (element, attrName) {
  pipeAttr(this.stream, element, attrName)
}

Sync.prototype.pipeAttr = function (element, attrName) {
  pipeAttr(this.stream, element, attrName)
}

// Pipe text
Async.prototype.pipeText = function (element) {
  pipeText(this.stream, element)
}

Sync.prototype.pipeText = function (element) {
  pipeText(this.stream, element)
}

// Pipe prop
Async.prototype.pipeProp = function (element, propName) {
  pipeProp(this.stream, element, propName)
}

Sync.prototype.pipeProp = function (element, propName) {
  pipeProp(this.stream, element, propName)
}

// Pipe children
Async.prototype.pipeChildren = function (element) {
  pipeChildren(this.stream, element)
  return this
}

Sync.prototype.pipeChildren = function (element) {
  pipeChildren(this.stream, element)
  return this
}

// Request animation frame
Async.prototype.frame = function (logTs) {
  const stream = frame(this.stream, logTs)
  this.stream = stream
  return this
}
