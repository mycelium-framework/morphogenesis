"use strict";

var _ = require(".");

require("./register");

var _async = require("hyphae/async");

var _porcini = require("porcini");

const sleep = ms => new Promise(resolve => setTimeout(() => resolve(), ms));

const stream = async function* (ms) {
  await sleep(ms);
  yield 'world';
  await sleep(ms);
  yield 'baz';
};

test('it should set the text as the async stream yields', async () => {
  const element = (0, _porcini.span)('hello');
  expect(element.textContent).toBe('hello');
  const delay = 200;
  (0, _.pipeText)(stream(delay), element);
  await sleep(delay + 10);
  expect(element.textContent).toBe('world');
  await sleep(delay - 20);
  expect(element.textContent).toBe('world');
  await sleep(delay + 10);
  expect(element.textContent).toBe('baz');
});
test('it should set the text after the given timeout', async () => {
  const element = (0, _porcini.span)('hello');
  const ms = 300;

  _async.Async.fromTimeout('world', ms).pipeText(element);

  expect(element.textContent).toBe('hello');
  await sleep(ms);
  expect(element.textContent).toBe('world');
});
test('it should set the text in a frame after the given timeout', async () => {
  const element = (0, _porcini.span)('hello');
  const element2 = (0, _porcini.span)('heya');
  const ms = 300;

  const stream = _async.Async.fromTimeout('world', ms).frame();

  const [stream2] = await stream.copy();
  stream.pipeText(element);
  stream2.pipeText(element2);
  expect(element.textContent).toBe('hello');
  expect(element2.textContent).toBe('heya');
  await sleep(ms + 100);
  expect(element.textContent).toBe('world');
  expect(element2.textContent).toBe('world');
});
test('it should set the attr as the async stream yields', async () => {
  const element = (0, _porcini.span)({
    className: 'hello'
  });
  expect(element.className).toBe('hello');
  const delay = 200;
  (0, _.pipeAttr)(stream(delay), element, 'class');
  await sleep(delay + 10);
  expect(element.className).toBe('world');
  await sleep(delay - 20);
  expect(element.className).toBe('world');
  await sleep(delay + 10);
  expect(element.className).toBe('baz');
}); // TODO: test child diffing