"use strict";

var _async = require("hyphae/async");

class MycelliumSpan extends HTMLSpanElement {
  constructor() {
    super();
  }

  get click() {
    return _async.Async.fromEvent('click', this);
  }

}

class MycelliumButton extends HTMLButtonElement {
  constructor() {
    super();
  }

  get click() {
    return _async.Async.fromEvent('click', this);
  }

}

class MyElem extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({
      mode: 'open'
    });
    const mbutton = new MycelliumButton();
    mbutton.textContent = 'Click me';
    const mspan = new MycelliumSpan();
    mbutton.click.map(_ => 'Hello').pipeText(mspan);
    shadow.appendChild(mbutton);
    shadow.appendChild(mspan);
  }

}

customElements.define('m-span', MycelliumSpan, {
  extends: 'span'
});
customElements.define('m-button', MycelliumButton, {
  extends: 'button'
});
customElements.define('my-elem', MyElem);