"use strict";

var _async = require("hyphae/async");

var _sync = require("hyphae/sync");

var _ = require(".");

// Pipe attribute
_async.Async.prototype.pipeAttr = function (element, attrName) {
  (0, _.pipeAttr)(this.stream, element, attrName);
};

_sync.Sync.prototype.pipeAttr = function (element, attrName) {
  (0, _.pipeAttr)(this.stream, element, attrName);
}; // Pipe text


_async.Async.prototype.pipeText = function (element) {
  (0, _.pipeText)(this.stream, element);
};

_sync.Sync.prototype.pipeText = function (element) {
  (0, _.pipeText)(this.stream, element);
}; // Pipe prop


_async.Async.prototype.pipeProp = function (element, propName) {
  (0, _.pipeProp)(this.stream, element, propName);
};

_sync.Sync.prototype.pipeProp = function (element, propName) {
  (0, _.pipeProp)(this.stream, element, propName);
}; // Pipe children


_async.Async.prototype.pipeChildren = function (element) {
  (0, _.pipeChildren)(this.stream, element);
  return this;
};

_sync.Sync.prototype.pipeChildren = function (element) {
  (0, _.pipeChildren)(this.stream, element);
  return this;
}; // Request animation frame


_async.Async.prototype.frame = function (logTs) {
  const stream = (0, _.frame)(this.stream, logTs);
  this.stream = stream;
  return this;
};