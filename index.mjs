import { fromPromise, flatten } from 'hyphae/async'

// It's a shallow differ, just tests for key matches and attaches
// any elements w/ new keys to the DOM
const childDiff = (node, newChilds) => {
  // Store all of the node's children in an object
  const children = {}
  for (let i = 0; i < node.childNodes.length; i++) {
    let child = node.childNodes[i]

    if (child instanceof HTMLElement && !child.hasAttribute('key'))
      throw new Error('Some children of the target element are not keyed')

    if (child instanceof HTMLElement) children[child.getAttribute('key')] = child
  }

  // If an existing child w/ same key exists then use that, otherwise use the new child
  const newChildren = []
  for (let i = 0; i < newChilds.length; i++) {
    let child = newChilds[i]

    if (child instanceof HTMLElement && !child.hasAttribute('key'))
      throw new Error('Some dynamic children are not keyed')

    const childToAppend =
      child instanceof HTMLElement && children[child.getAttribute('key')]
        ? children[child.getAttribute('key')]
        : child

    newChildren.push(childToAppend)
  }

  for (let i = 0; i < newChildren.length; i++) {
    // If it's the same node then ignore
    if (node.childNodes[i] === newChildren[i]) continue
    // If the node at that index is included later in the new nodes then don't detach
    else if (node.childNodes[i] && newChildren.includes(node.childNodes[i])) {
      if (newChildren[i] instanceof Text)
        node.childNodes[i].insertAdjacentText('beforebegin', newChildren[i].textContent)
      else node.childNodes[i].insertAdjacentElement('beforebegin', newChildren[i])
    }
    // Otherwise replace the hcild
    else if (node.childNodes[i]) node.replaceChild(newChildren[i], node.childNodes[i])
    // Or append the new child if it's new (not in the existing nodes)
    else node.appendChild(newChildren[i])
  }

  return node
}

const pipeAttr = async (stream, element, attrName) => {
  for await (let attrValue of stream) {
    element.setAttribute(attrName, attrValue)
  }
}

const pipeText = async (stream, element) => {
  for await (let text of stream) {
    element.textContent = text
  }
}

const pipeProp = async (stream, element, propName) => {
  for await (let propValue of stream) {
    element[propName] = propValue
  }
}

const pipeChildren = async (stream, element) => {
  for await (let children of stream) {
    childDiff(element, children)
  }
}

const frame = (stream, logTs) => {
  const promise = new Promise(resolve => {
    window.requestAnimationFrame(ts => {
      if (logTs) console.log({ ts })

      resolve(stream)
    })
  })

  return flatten(fromPromise(promise))
}

export { pipeAttr, pipeText, pipeProp, pipeChildren, frame }
