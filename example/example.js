import { pipeAttr, pipeText } from '..'
import '../register'
import '../elements'
import { Async } from 'hyphae/async'
import { span } from 'porcini'

const sleep = ms => new Promise(resolve => setTimeout(() => resolve(), ms))

;(async () => {
  const element = span('hello')
  const element2 = span('heya')

  document.body.appendChild(element)
  document.body.appendChild(element2)

  const ms = 300
  const stream = Async.fromTimeout('world', ms).frame()
  const [stream2] = await stream.copy()
  stream.pipeText(element)

  setTimeout(() => {
    stream2.pipeText(element2)
  }, 500)
})()


/*
const mbutton = new (customElements.get('m-button'))
mbutton.textContent = 'Click me'
const mspan = new (customElements.get('m-span'))

document.body.appendChild(mbutton)
document.body.appendChild(mspan)
*/

const mbutton = document.getElementById('my-button')
const mspan = document.getElementById('my-span')
mbutton.click.map(_ => 'Hello').pipeText(mspan)


/*
const element = span('hello')
const element2 = span('heya')

document.body.appendChild(element)
document.body.appendChild(element2)

setTimeout(() => {
  element.innerText = 'world'
}, 300)

setTimeout(() => {
  element2.innerText = 'world'
}, 500)
*/
